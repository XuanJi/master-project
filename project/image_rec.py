import cv2
import numpy as np

img = cv2.imread('test/red1.png')
hsv_image = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
# END HSV
# BEGIN FILTER
red_lower = np.array([160, 100, 100])
red_upper = np.array([180, 255, 255])
# blue
blue_lower = np.array([110, 100, 100])
blue_upper = np.array([130, 255, 255])
# mask

mask_red = cv2.inRange(hsv_image, red_lower, red_upper)
mask_blue = cv2.inRange(hsv_image, blue_lower, blue_upper)
# mask_img = cv2.inRange(img, blue_lower, blue_upper)
mask = cv2.bitwise_or(mask_red, mask_blue)
print(np.all(mask == mask_blue))

# (_, cnts, _) = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# M = cv2.moments(mask)
# if M['m00'] > 0:
#     cx = int(M['m10'] / M['m00'])
#     cy = int(M['m01'] / M['m00'])
#
#     # cx range (55,750) cy range( 55, ~ )
#     # END FINDER
#     # Isolate largest contour
#     #  contour_sizes = [(cv2.contourArea(contour), contour) for contour in cnts]
#     #  biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
#     for i, c in enumerate(cnts):
#         area = cv2.contourArea(c)
#         if area > 7500:
#             cv2.circle(img, (cx, cy), 5, (0, 0, 0), -1)
#             if np.all(mask == mask_red):
#                 cv2.putText(img, "cube", (int(cx - 5), int(cy + 15)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
#             elif np.all(mask == mask_blue):
#                 cv2.putText(img, "cylinder", (int(cx - 5), int(cy + 15)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
#             # cv2.drawContours(img, cnts, -1, (255, 255, 255), 1)

cv2.imshow('demo', mask)
cv2.imshow('origin', img)
cv2.waitKey()
cv2.destroyAllWindows()
