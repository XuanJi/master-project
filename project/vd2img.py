#####将视频分解为图片

import cv2
import numpy as np

cap = cv2.VideoCapture("test.mp4")
isOpened = cap.isOpened()  ##判断视频是否打开
print(isOpened)
fps = cap.get(cv2.CAP_PROP_FPS)  ##获取帧率
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))  ###获取宽度
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))  ###获取高度
print(fps, width, height)
num = 0
while isOpened:
    num = num + 1
    (flag, frame) = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # END HSV
    # BEGIN FILTER
    hsv_red_lower = np.array([0, 100, 100])
    hsv_red_upper = np.array([10, 255, 255])
    # blue
    hsv_blue_lower = np.array([110, 100, 100])
    hsv_blue_upper = np.array([130, 255, 255])
    # mask
    mask_hsv_red = cv2.inRange(hsv, hsv_red_lower, hsv_red_upper)
    mask_hsv_blue = cv2.inRange(hsv, hsv_blue_lower, hsv_blue_upper)

    mask = cv2.bitwise_or(mask_hsv_red, mask_hsv_blue)
    (_, cnts, _) = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    # area = cv2.contourArea(cnts)
    for i, c in enumerate(cnts):
        area = cv2.contourArea(c)
        if area > 7500:
            fileName = "image" + str(num) + ".jpg"
            print(fileName)
            if flag == True:
                cv2.imwrite("image/image" + str(num) + ".jpg", frame,
                            [cv2.IMWRITE_JPEG_CHROMA_QUALITY, 100])  ##命名 图片 图片质量
print("end!")
