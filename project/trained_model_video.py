import cv2
import numpy as np
import torch
from PIL import Image
from torchvision import transforms
import torch.nn as nn


class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 32, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(32),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
        )

        self.conv2 = nn.Sequential(
            nn.Conv2d(32, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        # self.conv4 = nn.Sequential(
        #     nn.Conv2d(64, 128, 3),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(128),
        #     nn.ReLU(),  # activation
        #     nn.MaxPool2d(2),  # kernel size
        #     nn.Dropout(0.5)
        # )
        # #
        # self.conv5 = nn.Sequential(
        #     nn.Conv2d(128, 256, 3, padding=1),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(),  # activation
        #     nn.Conv2d(256, 256, 3),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(),  # activation
        #     nn.MaxPool2d(2),  # kernel size
        #     nn.Dropout(0.5)
        # )

        self.out1 = nn.Linear(64 * 30 * 30, 512)
        self.out2 = nn.Linear(512, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        # x = self.conv4(x)
        # x = self.conv5(x)

        x = x.view(x.size(0), -1)
        x = self.out1(x)
        out = self.out2(x)
        return out


mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

classes = ['cube', 'cylinder']

device = torch.device('cuda')
transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(256),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])

net = torch.load('model.pth')
net = net.to(device)
torch.no_grad()


def prediect(img_path):
    img = Image.fromarray(img_path).convert('RGB')
    img = transform(img).unsqueeze(0)
    img_ = img.to(device)
    outputs = net(img_)
    _, predicted = torch.max(outputs, 1)
    out = predicted.cpu().numpy()[0]
    print('this picture maybe :', classes[out])
    # cv2.circle(frame, (cx, cy), 5, (0, 0, 0), -1)
    # cv2.putText(frame, classes[out], (int(cx - 5), int(cy + 15)),
    #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)



videoCapture = cv2.VideoCapture('test.mp4')

# 获得码率及尺寸
fps = videoCapture.get(cv2.CAP_PROP_FPS)
size = (int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
fNUMS = videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)

# 读帧
success, frame = videoCapture.read()
while success:
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # END HSV
    # BEGIN FILTER
    hsv_red_lower = np.array([0, 100, 100])
    hsv_red_upper = np.array([10, 255, 255])
    # blue
    hsv_blue_lower = np.array([110, 100, 100])
    hsv_blue_upper = np.array([130, 255, 255])
    # mask
    mask_hsv_red = cv2.inRange(hsv, hsv_red_lower, hsv_red_upper)
    mask_hsv_blue = cv2.inRange(hsv, hsv_blue_lower, hsv_blue_upper)

    mask = cv2.bitwise_or(mask_hsv_red, mask_hsv_blue)
    (_, cnts, _) = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # area = cv2.contourArea(cnts)
    h, w, d = frame.shape
    # print h, w, d  (800,800,3)
    # BEGIN FINDER
    M = cv2.moments(mask)
    if M['m00'] > 0:
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        for i, c in enumerate(cnts):
            area = cv2.contourArea(c)
            if area > 7500:
                prediect(mask)
                # cv2.circle(frame, (cx, cy), 5, (0, 0, 0), -1)
                # cv2.putText(frame, "({}, {})".format(int(cx), int(cy)), (int(cx - 5), int(cy + 15)),
                #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
                # cv2.drawContours(frame, cnts, -1, (255, 255, 255), 1)

    cv2.imshow('windows', frame)  # 显示
    cv2.waitKey(int(1000/fps))  # 延迟
    success, frame = videoCapture.read()  # 获取下一帧

videoCapture.release()
