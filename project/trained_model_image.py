import torch
from PIL import Image
from torchvision import transforms
import torch.nn as nn


class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 32, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(32),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
        )

        self.conv2 = nn.Sequential(
            nn.Conv2d(32, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        # self.conv4 = nn.Sequential(
        #     nn.Conv2d(64, 128, 3),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(128),
        #     nn.ReLU(),  # activation
        #     nn.MaxPool2d(2),  # kernel size
        #     nn.Dropout(0.5)
        # )
        # #
        # self.conv5 = nn.Sequential(
        #     nn.Conv2d(128, 256, 3, padding=1),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(),  # activation
        #     nn.Conv2d(256, 256, 3),  # input channels, output channels, kernel size
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(),  # activation
        #     nn.MaxPool2d(2),  # kernel size
        #     nn.Dropout(0.5)
        # )

        self.out1 = nn.Linear(64 * 30 * 30, 512)
        self.out2 = nn.Linear(512, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        # x = self.conv4(x)
        # x = self.conv5(x)

        x = x.view(x.size(0), -1)
        x = self.out1(x)
        out = self.out2(x)
        return out


mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

classes = ['cube', 'cylinder']

device = torch.device('cuda')
transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(256),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])

net = torch.load('model.pth')
net = net.to(device)
torch.no_grad()


def prediect(img_path):
    img = Image.open(img_path).convert('RGB')
    img = transform(img).unsqueeze(0)
    img_ = img.to(device)
    outputs = net(img_)
    _, predicted = torch.max(outputs, 1)
    out = predicted.cpu().numpy()[0]
    print('this picture maybe :', classes[out])


if __name__ == '__main__':
    prediect('demo_cube.png')
    # prediect('test1.png')
    # prediect('test2.png')
    # prediect('test3.png')
    # prediect('test4.png')
