import cv2
import numpy as np

videoCapture = cv2.VideoCapture('test.mp4')

# 获得码率及尺寸
fps = videoCapture.get(cv2.CAP_PROP_FPS)
size = (int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
fNUMS = videoCapture.get(cv2.CAP_PROP_FRAME_COUNT)

# 读帧
success, frame = videoCapture.read()
while success:
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # END HSV
    # BEGIN FILTER
    hsv_red_lower = np.array([0, 100, 100])
    hsv_red_upper = np.array([10, 255, 255])
    # blue
    hsv_blue_lower = np.array([110, 100, 100])
    hsv_blue_upper = np.array([130, 255, 255])
    # mask
    mask_hsv_red = cv2.inRange(hsv, hsv_red_lower, hsv_red_upper)
    mask_hsv_blue = cv2.inRange(hsv, hsv_blue_lower, hsv_blue_upper)

    mask = cv2.bitwise_or(mask_hsv_red, mask_hsv_blue)
    (_, cnts, _) = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    M = cv2.moments(mask)
    if M['m00'] > 0:
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])

        # cx range (55,750) cy range( 55, ~ )
        # END FINDER
        # Isolate largest contour
        #  contour_sizes = [(cv2.contourArea(contour), contour) for contour in cnts]
        #  biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
        for i, c in enumerate(cnts):
            area = cv2.contourArea(c)
            if area > 7500:
                # cv2.circle(frame, (cx, cy), 5, (0, 0, 0), -1)
                if np.all(mask_hsv_red == mask):
                    cv2.putText(frame, "Cube", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1,
                                (255, 255, 255), 2)
                elif np.all(mask_hsv_blue == mask):
                    cv2.putText(frame, "Cylinder", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                # cv2.putText(frame, "({}, {})".format(int(cx), int(cy)), (int(cx - 100), int(cy + 50)),
                #             cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                # cv2.drawContours(frame, cnts, -1, (255, 255, 255), 1)
    # area = cv2.contourArea(cnts)

    cv2.imshow('windows', frame)  # 显示
    cv2.waitKey(int(1000 / fps))  # 延迟
    success, frame = videoCapture.read()  # 获取下一帧

videoCapture.release()
