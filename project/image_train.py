import torch
from torch import optim
import torch.nn as nn
import torchvision.transforms as transforms
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
from tqdm import tqdm
from PIL import Image
import pandas as pd
import numpy as np
import os
import cv2

root_dir = "dataset2/"
class_names = ['cube', 'cylinder']


def get_meta(root_dir, dirs):
    """ Fetches the meta data for all the images and assigns labels.
    """
    paths, classes = [], []
    for i, dir_ in enumerate(dirs):
        for entry in os.scandir(root_dir + dir_):
            if entry.is_file():
                paths.append(entry.path)
                classes.append(i)

    return paths, classes


paths, classes = get_meta(root_dir, class_names)

data = {
    'path': paths,
    'class': classes
}

data_df = pd.DataFrame(data, columns=['path', 'class'])
data_df = data_df.sample(frac=1).reset_index(drop=True)  # Shuffles the data

print("Found", len(data_df), "images.")
data_df.head()


class ImageNet10(Dataset):
    def __init__(self, df, transform=None):
        self.df = df
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        # Load image from path and get label
        x = Image.open(self.df['path'][index])
        try:
            x = x.convert('RGB')  # To deal with some grayscale images in the data
        except:
            pass
        y = torch.tensor(int(self.df['class'][index]))

        if self.transform:
            x = self.transform(x)

        return x, y


def compute_img_mean_std(image_paths):
    img_h, img_w = 224, 224
    imgs = []
    means, stdevs = [], []

    for i in tqdm(range(len(image_paths))):
        img = cv2.imread(image_paths[i])
        img = cv2.resize(img, (img_h, img_w))
        imgs.append(img)

    imgs = np.stack(imgs, axis=3)
    print(imgs.shape)

    imgs = imgs.astype(np.float32) / 255.

    for i in range(3):
        pixels = imgs[:, :, i, :].ravel()  # resize to one row
        means.append(np.mean(pixels))
        stdevs.append(np.std(pixels))

    means.reverse()  # BGR --> RGB
    stdevs.reverse()

    print("normMean = {}".format(means))
    print("normStd = {}".format(stdevs))
    return means, stdevs


norm_mean, norm_std = compute_img_mean_std(paths)
data_transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(256),
    transforms.ToTensor(),
    transforms.Normalize(norm_mean, norm_std),
])

train_split = 0.90
test_split = 0.10
train_size = int(len(data_df) * train_split)
test_size = int(len(data_df) * test_split)

ins_dataset_train = ImageNet10(
    df=data_df[:train_size],
    transform=data_transform,
)

ins_dataset_test = ImageNet10(
    df=data_df[:test_size].reset_index(drop=True),
    transform=data_transform,
)

train_loader = torch.utils.data.DataLoader(
    ins_dataset_train,
    batch_size=16,
    shuffle=True,
    num_workers=0
)

test_loader = torch.utils.data.DataLoader(
    ins_dataset_test,
    batch_size=24,
    shuffle=False,
    num_workers=0
)

classes = np.arange(0, 10)


class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 32, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(32),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
        )

        self.conv2 = nn.Sequential(
            nn.Conv2d(32, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, 3),  # input channels, output channels, kernel size
            nn.BatchNorm2d(64),
            nn.ReLU(),  # activation
            nn.MaxPool2d(2),  # kernel size
            nn.Dropout(0.5)
        )

        self.out1 = nn.Linear(64 * 30 * 30, 512)
        self.out2 = nn.Linear(512, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)

        x = x.view(x.size(0), -1)
        x = self.out1(x)
        out = self.out2(x)
        return out


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
model_gpu = ConvNet().to(device)
criterion = nn.CrossEntropyLoss()
# Stochastic gradient descent
optimizer = optim.SGD(model_gpu.parameters(), lr=0.01, momentum=0.9)
# start_data = model_gpu.conv1[0].weight.cpu().detach().numpy()

epochs = 10
train_loss_history = []
train_correct_history = []

for e in range(epochs):
    loss = 0.0
    correct = 0.0
    for input, labels in train_loader:
        input = input.to(device)
        labels = labels.to(device)
        outputs = model_gpu(input)
        loss1 = criterion(outputs, labels)
        optimizer.zero_grad()
        loss1.backward()
        optimizer.step()
        _, preds = torch.max(outputs, 1)
        loss += loss1.item()
        correct += torch.sum(preds == labels.data)

    epoch_loss = loss / len(train_loader)
    epoch_acc = float(correct) / train_size
    train_loss_history.append(epoch_loss)
    train_correct_history.append(epoch_acc)
    print(' ')
    print("Current epoch: ", e + 1)
    print('training_loss: {:.4f}, accuracy: {:.4f}'.format(epoch_loss, epoch_acc))
torch.save(model_gpu, 'model.pth')
plt.plot(train_loss_history, label='Training Loss')
plt.plot(train_correct_history, label='Training Accuracy')
plt.legend()
plt.show()

correct = 0
total = 0

final_label = []
final_predicted = []
with torch.no_grad():
    # Iterate over the test set
    for images, labels in test_loader:
        images = images.to(device)  # missing line from original code
        labels = labels.to(device)  # missing line from original code

        outputs = model_gpu(images)
        final_label = np.append(final_label, labels.cpu())

        # torch.max is an argmax operation
        _, predicted = torch.max(outputs.data, 1)

        total += labels.size(0)
        correct += (predicted == labels).sum().item()
        final_predicted = np.append(final_predicted, predicted.cpu())
print('Accuracy of the network on the test images: %d %%' % (100 * correct / total))
