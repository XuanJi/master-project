# Master project

Method to use helper file
1. mkdir -p rob_ws/src
2. put helper file into src path
3. git clone -b kinetic-devel https://github.com/ros-industrial/universal_robot.git (also under src path)
4. put three files (in the helper/robot folder) into rob_ws/src/universal_robot/ur_description/urdf/ path
5. back to src path
6. catkin_make
7. source devel/setup.bash
8. roslaunch ur5_notebook initialize.launch
9. roslaunch ur5_moveit_config moveit_rviz.launch config:=true

The file in the projcet folder is used for object recognition.
